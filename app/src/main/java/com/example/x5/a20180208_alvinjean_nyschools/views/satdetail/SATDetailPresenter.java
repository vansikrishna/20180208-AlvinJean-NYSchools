package com.example.x5.a20180208_alvinjean_nyschools.views.satdetail;

import android.util.Log;

import com.example.x5.a20180208_alvinjean_nyschools.models.SATScore;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SATDetailPresenter implements SATDetailContract.Presenter {
    private static final String TAG = SATDetailPresenter.class.getSimpleName() + "_TAG";
    private SATDetailContract.View view;
    @Inject
    OkHttpClient client;
    private List<SATScore> satScoreList;

    @Inject
    public SATDetailPresenter(OkHttpClient client) {
        this.client = client;
    }

    @Override
    public void attachView(SATDetailContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    //Retrieve the SAT Scores from the JSON API URL
    @Override
    public void getSATData(final String highSchoolDBN) {
        Log.d(TAG, "getSATData: Getting DATA FOR SAT DBN: " + highSchoolDBN);
        final Request request = new Request.Builder().url("https://data.cityofnewyork.us/resource/734v-jeq5.json").build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String json = response.body().string();
                Gson gson = new Gson();

                Type collectionType = new TypeToken<Collection<SATScore>>(){}.getType();
                Collection<SATScore> enums = gson.fromJson(json, collectionType);
                //Convert the incoming JSON data into an ArrayList and find the corresponding Scores
                satScoreList = new ArrayList<>(enums);
                SATScore scoreToShow = new SATScore();
                //Find the SAT scores based on the DBN of the school selected
                //If nothing is found, notify the user in the UI
                scoreToShow.setSchoolName("NO SAT INFORMATION AVAILABLE");
                for(SATScore satScore: satScoreList) {
                    if(satScore.getDbn().equals(highSchoolDBN)){
                        scoreToShow = satScore;
                        break;
                    }
                }
                view.showScores(scoreToShow);
            }
        });
    }
}
